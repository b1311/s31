// Contain all task endpoints for our applications

const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

// route for getting all tasks
router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// route for creating task

router.post('/createTask', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// route for deleting task

router.delete('/deleteTask/:id', (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})



//mini activity

// Getting specific task

router.get('/specificTask/:id', (req, res) => {

	taskController.specificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// Changing status

router.put('/statusTask/:id', (req, res) => {

	taskController.statusTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router