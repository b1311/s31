const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://admin:admin123@zuittbootcamp.4mcba.mongodb.net/session30?retryWrites=true&w=majority',
	

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

//console.error.bind(console)- print errors in the browser and in the terminal
db.on("error", console.error.bind(console, "Connection Error"));

// if the connection is successful, this will be the output in our console.
db.once("open", () => console.log("Successfully connected to MongoDB"));

app.use('/tasks', taskRoutes)


app.listen(port, () => console.log(`Server running at port ${port}`));